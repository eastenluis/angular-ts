# Custom Implementation of AngularJS in TypeScript

This is an exercise that build a custom AngularJS following the instructions from [Build Your Own AngularJS](http://teropa.info/build-your-own-angular/).

I decided to use TypeScript instead so that I can have a better practice on TypeScript. I don't expect it to have a good performance in practice.