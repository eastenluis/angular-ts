var webpackConfig = require('./webpack.test.js');

module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],
        files: [
            'test/**/*.spec.ts'
        ],
        preprocessors: {
            'test/**/*.spec.ts': ['webpack']
        },
        webpack: webpackConfig,
        webpackMiddleware: {
            noInfo: true
        },
        browsers: ['PhantomJS'],
        port: 9876,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        singleRun: true
    });
};