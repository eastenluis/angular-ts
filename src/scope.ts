import * as _ from 'lodash';

interface Watcher {
    listenerFn: Function;
    watchFn: Function;
    last?: any;
}

// Ensure watcher.last is always different from any value,
// so it's always called when the watcher reach its first digest.
const uniqueInitVal = function () { };

export class Scope {
    private $$watchers: Array<Watcher> = [];
    private $$lastDigestWatcher: Watcher;
    $watch(watchFn: Function, listenerFn?: Function) {
        let watcher: Watcher = {
            watchFn: watchFn,
            listenerFn: listenerFn,
            last: uniqueInitVal
        };
        this.$$watchers.push(watcher);
        this.$$lastDigestWatcher = null;
    }
    $$digestOnce(): boolean {
        let dirty = false;
        _.forEach(this.$$watchers, (watcher) => {
            let newValue = watcher.watchFn(this);
            let oldValue = watcher.last;
            if (watcher.listenerFn && newValue !== oldValue) {
                this.$$lastDigestWatcher = watcher;
                watcher.last = newValue;
                watcher.listenerFn(newValue, (oldValue === uniqueInitVal ? newValue : oldValue), this);
                dirty = true;
            } else if (this.$$lastDigestWatcher === watcher) {
                // A full round digest loop has been made with all watchers clean.
                // Exit the loop.
                return false;
            }
        });
        return dirty;
    }
    $digest() {
        let dirty = true;
        let ttl = 10;
        this.$$lastDigestWatcher = null;
        do {
            dirty = this.$$digestOnce();
            if (dirty && !(ttl--)) {
                throw 'Maximum digest reached.'
            }
        } while (dirty);
    }
}
