import {Scope} from '../src/scope.ts';
import * as _ from 'lodash';

describe('Hello', () => {
  it("can be constructed and used as an object", function () {
    let scope: any = new Scope();
    scope.aProperty = 1;
    expect(scope.aProperty).toBe(1);
  });

  describe("Digest", () => {
    let scope: any;

    beforeEach(function () {
      scope = new Scope();
    });

    it("calls the listener function of a watch on first $digest", () => {
      let watchFn = function () { return "wat"; };
      let listenerFn = jasmine.createSpy("listenerFn");
      scope.$watch(watchFn, listenerFn);
      scope.$digest();

      expect(listenerFn).toHaveBeenCalled();
    });

    it("calls the watch function with the scope as the argument", function () {
      let watchFn = jasmine.createSpy('watchFunc');
      let listenerFn = function () { };
      scope.$watch(watchFn, listenerFn);
      scope.$digest();

      expect(watchFn).toHaveBeenCalledWith(scope);
    });

    it("calls the listener function when the watched value changes", function () {
      scope.someValue = 'a';
      scope.counter = 0;
      scope.$watch(
        function (scope: any) { return scope.someValue; },
        function (newValue: any, oldValue: any, scope: any) { scope.counter++; }
      );
      expect(scope.counter).toBe(0);
      scope.$digest();
      expect(scope.counter).toBe(1);
      scope.$digest();
      expect(scope.counter).toBe(1);
      scope.someValue = 'b';
      expect(scope.counter).toBe(1);
      scope.$digest();
      expect(scope.counter).toBe(2);
    });

    it("calls listener when watch value is first undefined", function () {
      scope.counter = 0;
      scope.$watch(
        function (scope: any) { return scope.someValue; },
        function (newValue: any, oldValue: any, scope: any) { scope.counter++; }
      );
      scope.$digest();
      expect(scope.counter).toBe(1);
    });

    it("calls listener with new value as old value the first time", function () {
      scope.someValue = 123;
      let oldValueGiven: any;
      scope.$watch(
        function (scope: any) { return scope.someValue; },
        function (newValue: any, oldValue: any, scope: any) { oldValueGiven = oldValue; }
      );
      scope.$digest();
      expect(oldValueGiven).toBe(123);
    });

    it("may have watchers that omit the listener function", function () {
      var watchFn = jasmine.createSpy("watchOnly").and.returnValue('something');
      scope.$watch(watchFn);
      scope.$digest();
      expect(watchFn).toHaveBeenCalled();
    });

    it("triggers chained watchers in the same digest", function () {
      scope.name = 'Jane';
      scope.$watch(
        (scope: any) => scope.nameUpper,
        (newValue: any, oldValue: any, scope: any) => {
          if (newValue) {
            scope.initial = newValue.substring(0, 1) + '.';
          }
        });
      scope.$watch(
        (scope: any) => scope.name,
        (newValue: any, oldValue: any, scope: any) => {
          if (newValue) {
            scope.nameUpper = newValue.toUpperCase();
          }
        });
      scope.$digest();
      expect(scope.initial).toBe('J.');
      scope.name = 'Bob';
      scope.$digest();
      expect(scope.initial).toBe('B.');
    });

    it("gives up on the watches after 10 iterations", function () {
      scope.counterA = 0;
      scope.counterB = 0;
      scope.$watch(
        (scope: any) => scope.counterA,
        (newValue: any, oldValue: any, scope: any) => {
          scope.counterB++;
        }
      );
      scope.$watch(
        (scope: any) => scope.counterB,
        (newValue: any, oldValue: any, scope: any) => {
          scope.counterA++;
        }
      );
      expect((function () { scope.$digest(); })).toThrow();
    });

    it("ends the digest when the last watch is clean", () => {
      scope.array = _.range(100);
      var watchExecutions = 0;
      _.times(100, function (i) {
        scope.$watch(
          (scope: any) => {
            watchExecutions++;
            return scope.array[i];
          },
          (newValue: any, oldValue: any, scope: any) => { });
      });
      scope.$digest();
      expect(watchExecutions).toBe(200);
      scope.array[0] = 420;
      scope.$digest();
      expect(watchExecutions).toBe(301);
    });

    it("does not end digest so that new watches are not run", function () {
      scope.aValue = 'abc';
      scope.counter = 0;
      scope.$watch(
        (scope: any) => scope.aValue,
        (newValue: any, oldValue: any, scope: any) => {
          scope.$watch(
            (scope: any) => scope.aValue,
            (newValue: any, oldValue: any, scope: any) => {
              scope.counter++;
            }
          );
        });
      scope.$digest();
      expect(scope.counter).toBe(1);
    });
  });
});