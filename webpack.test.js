var path = require("path");

function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [__dirname].concat(args));
}

module.exports = {  
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js'],
    root: root('src')
  },
  module: {
    loaders: [
      { test: /\.ts$/, loader: 'ts-loader' }
    ]
  }
};